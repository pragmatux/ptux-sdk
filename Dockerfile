FROM debian:stretch
LABEL maintainer rkuester@insymbols.com

ARG DEBIAN_FRONTEND=noninteractive

# Fix obsolete sources.list in FROM image and bring up to date
RUN echo "deb http://archive.debian.org/debian stretch main" >/etc/apt/sources.list \
 && echo "deb http://archive.debian.org/debian-security stretch/updates main" >>/etc/apt/sources.list \
 && apt-get update \
 && apt-get -yq upgrade

# Install ptux-sdk
COPY debian/build/*.deb /tmp/
RUN echo "deb [trusted=yes] http://packages.pragmatux.org/sdk stretch main" >/etc/apt/sources.list.d/pragmatux.list \
 && apt-get update \
 && apt-get -yq upgrade \
 && apt-get install -yq /tmp/*.deb
